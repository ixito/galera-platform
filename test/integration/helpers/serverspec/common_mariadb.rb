#
# Copyright (c) 2015-2016 Sam4Mobile
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'

describe 'MariaDB Daemon' do
  it 'is running' do
    expect(service('mariadb')).to be_running
  end

  it 'is launched at boot' do
    expect(service('mariadb')).to be_enabled
  end

  it 'is listening on correct port' do
    expect(port(3306)).to be_listening
  end
end

describe file('/etc/my.cnf.d/server.cnf') do
  its(:content) { should contain 'bind-address=0.0.0.0' }
  its(:content) { should contain 'wsrep_cluster_name=MariaDB_Cluster' }
  its(:content) { should contain 'wsrep_sst_method=xtrabackup' }
end

mysql_cmd_status = 'mysql -Ne "show status like \'wsrep_local_state_comment\'"'
mysql_cmd_cluster_size = 'mysql -Ne "show status like \'wsrep_cluster_size\'"'
describe 'Cluster' do
  it 'should be synced' do
    exp = "wsrep_local_state_comment\tSynced\n"
    result = `#{mysql_cmd_status}`
    expect(result).to eq(exp)
  end

  it 'size should be 3' do
    exp = "wsrep_cluster_size\t3\n"
    result = `#{mysql_cmd_cluster_size}`
    expect(result).to eq(exp)
  end
end
